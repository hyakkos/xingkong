const json = require("@rollup/plugin-json");
const { terser } = require('rollup-plugin-terser');
const resolve = require('@rollup/plugin-node-resolve');
const babel = require('@rollup/plugin-babel');
const commonjs = require('@rollup/plugin-commonjs');
exports.default = {
  input: 'src/index.js',
  output: [
    {
      file: 'dist/xingkong.js',
      format: 'umd',
      name: 'xingkong',
    },
    {
      file: 'dist/xingkong.min.js',
      format: 'umd',
      name: 'xingkong',
      plugins: [terser()]
    }
  ],
  plugins: [
    commonjs(),
    json(),
    resolve(),
    babel({ babelHelpers: 'bundled' })
  ]
};