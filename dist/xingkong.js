(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.xingkong = {}));
})(this, (function (exports) { 'use strict';

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor);
    }
  }
  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    Object.defineProperty(Constructor, "prototype", {
      writable: false
    });
    return Constructor;
  }
  function _defineProperty(obj, key, value) {
    key = _toPropertyKey(key);
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }
    return obj;
  }
  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    Object.defineProperty(subClass, "prototype", {
      writable: false
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }
  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }
  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };
    return _setPrototypeOf(o, p);
  }
  function _isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;
    try {
      Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }
  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }
    return self;
  }
  function _possibleConstructorReturn(self, call) {
    if (call && (typeof call === "object" || typeof call === "function")) {
      return call;
    } else if (call !== void 0) {
      throw new TypeError("Derived constructors may only return object or undefined");
    }
    return _assertThisInitialized(self);
  }
  function _createSuper(Derived) {
    var hasNativeReflectConstruct = _isNativeReflectConstruct();
    return function _createSuperInternal() {
      var Super = _getPrototypeOf(Derived),
        result;
      if (hasNativeReflectConstruct) {
        var NewTarget = _getPrototypeOf(this).constructor;
        result = Reflect.construct(Super, arguments, NewTarget);
      } else {
        result = Super.apply(this, arguments);
      }
      return _possibleConstructorReturn(this, result);
    };
  }
  function _toPrimitive(input, hint) {
    if (typeof input !== "object" || input === null) return input;
    var prim = input[Symbol.toPrimitive];
    if (prim !== undefined) {
      var res = prim.call(input, hint || "default");
      if (typeof res !== "object") return res;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return (hint === "string" ? String : Number)(input);
  }
  function _toPropertyKey(arg) {
    var key = _toPrimitive(arg, "string");
    return typeof key === "symbol" ? key : String(key);
  }

  // 0A 提示 0B 警告 0C 错误
  var ERRORCODE = {
    "0A000001": "不存在的值，使用的默认值替代",
    "0B000001": "无效的入参",
    "0C000001": "无效的错误码",
    "0C000002": "无效的挂载节点"
  };

  var Logger = /*#__PURE__*/function () {
    function Logger() {
      _classCallCheck(this, Logger);
    }
    _createClass(Logger, null, [{
      key: "info",
      value: function info(message) {
        console.info(message);
      }
    }, {
      key: "warn",
      value: function warn(message) {
        console.warn(message);
      }
    }, {
      key: "error",
      value: function error(message) {
        console.error(message);
      }
    }, {
      key: "log",
      value: function log(message) {
        console.log(message);
      }
    }, {
      key: "logCode",
      value: function logCode(code) {
        if (code && code.length === 8) {
          var classify = code.slice(0, 2);
          var message = ERRORCODE[code] || undefined;
          if (message) {
            if (classify === "0A") {
              Logger.info("[".concat(code, "]").concat(message));
            } else if (classify === "0B") {
              Logger.warn("[".concat(code, "]").concat(message));
            } else {
              Logger.error("[".concat(code, "]").concat(message));
            }
          } else {
            Logger.logCode(ERRORCODE["0C000001"]);
          }
        } else {
          Logger.logCode(ERRORCODE["0B000001"]);
        }
      }
    }]);
    return Logger;
  }();

  var EVENTTYPE = {
    RESIZE: "resize"
  };

  var jevent_min = {exports: {}};

  (function (module, exports) {
    !function (t, e) {
      module.exports = e();
    }(window, function () {
      return function (t) {
        var e = {};
        function n(r) {
          if (e[r]) return e[r].exports;
          var o = e[r] = {
            i: r,
            l: !1,
            exports: {}
          };
          return t[r].call(o.exports, o, o.exports, n), o.l = !0, o.exports;
        }
        return n.m = t, n.c = e, n.d = function (t, e, r) {
          n.o(t, e) || Object.defineProperty(t, e, {
            enumerable: !0,
            get: r
          });
        }, n.r = function (t) {
          "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
            value: "Module"
          }), Object.defineProperty(t, "__esModule", {
            value: !0
          });
        }, n.t = function (t, e) {
          if (1 & e && (t = n(t)), 8 & e) return t;
          if (4 & e && "object" == typeof t && t && t.__esModule) return t;
          var r = Object.create(null);
          if (n.r(r), Object.defineProperty(r, "default", {
            enumerable: !0,
            value: t
          }), 2 & e && "string" != typeof t) for (var o in t) n.d(r, o, function (e) {
            return t[e];
          }.bind(null, o));
          return r;
        }, n.n = function (t) {
          var e = t && t.__esModule ? function () {
            return t.default;
          } : function () {
            return t;
          };
          return n.d(e, "a", e), e;
        }, n.o = function (t, e) {
          return Object.prototype.hasOwnProperty.call(t, e);
        }, n.p = "", n(n.s = 0);
      }([function (t, e, n) {

        n.r(e), n.d(e, "JEvent", function () {
          return i;
        }), n.d(e, "JCustomEvent", function () {
          return y;
        }), n.d(e, "JErrorEvent", function () {
          return w;
        }), n.d(e, "JEventTarget", function () {
          return _;
        });
        function r(t) {
          return (r = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (t) {
            return typeof t;
          } : function (t) {
            return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
          })(t);
        }
        function o(t, e) {
          for (var n = 0; n < e.length; n++) {
            var o = e[n];
            o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(t, (i = o.key, u = void 0, u = function (t, e) {
              if ("object" !== r(t) || null === t) return t;
              var n = t[Symbol.toPrimitive];
              if (void 0 !== n) {
                var o = n.call(t, e || "default");
                if ("object" !== r(o)) return o;
                throw new TypeError("@@toPrimitive must return a primitive value.");
              }
              return ("string" === e ? String : Number)(t);
            }(i, "string"), "symbol" === r(u) ? u : String(u)), o);
          }
          var i, u;
        }
        var i = function () {
          function t(e, n) {
            var r, o, i;
            !function (t, e) {
              if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
            }(this, t), this.bubbles = !1, this.cancelable = !1, this.currentTarget = null, this.defaultPrevented = !1, this.eventPhase = 0, this.target = null, this.timeStamp = Date.now(), this.type = null, this.propagationStopped = !1, this.immediatePropagationStopped = !1, this.type = e, this.bubbles = null !== (r = null == n ? void 0 : n.bubbles) && void 0 !== r ? r : this.bubbles, this.cancelable = null !== (o = null == n ? void 0 : n.cancelable) && void 0 !== o ? o : this.cancelable, this.composed = null !== (i = null == n ? void 0 : n.composed) && void 0 !== i ? i : this.composed;
          }
          var e, n;
          return e = t, (n = [{
            key: "preventDefault",
            value: function () {
              this.defaultPrevented = this.cancelable && !0;
            }
          }, {
            key: "stopPropagation",
            value: function () {
              this.propagationStopped = !0;
            }
          }, {
            key: "stopImmediatePropagation",
            value: function () {
              this.stopPropagation(), this.immediatePropagationStopped = !0;
            }
          }, {
            key: "toString",
            value: function () {
              return "[JEvent (type=" + this.type + ")]";
            }
          }]) && o(e.prototype, n), Object.defineProperty(e, "prototype", {
            writable: !1
          }), t;
        }();
        function u(t) {
          return (u = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (t) {
            return typeof t;
          } : function (t) {
            return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
          })(t);
        }
        function l(t, e) {
          if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        function c(t, e) {
          for (var n = 0; n < e.length; n++) {
            var r = e[n];
            r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(t, (o = r.key, i = void 0, i = function (t, e) {
              if ("object" !== u(t) || null === t) return t;
              var n = t[Symbol.toPrimitive];
              if (void 0 !== n) {
                var r = n.call(t, e || "default");
                if ("object" !== u(r)) return r;
                throw new TypeError("@@toPrimitive must return a primitive value.");
              }
              return ("string" === e ? String : Number)(t);
            }(o, "string"), "symbol" === u(i) ? i : String(i)), r);
          }
          var o, i;
        }
        function a(t, e) {
          return (a = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function (t, e) {
            return t.__proto__ = e, t;
          })(t, e);
        }
        function f(t) {
          var e = function () {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
              return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})), !0;
            } catch (t) {
              return !1;
            }
          }();
          return function () {
            var n,
              r = p(t);
            if (e) {
              var o = p(this).constructor;
              n = Reflect.construct(r, arguments, o);
            } else n = r.apply(this, arguments);
            return s(this, n);
          };
        }
        function s(t, e) {
          if (e && ("object" === u(e) || "function" == typeof e)) return e;
          if (void 0 !== e) throw new TypeError("Derived constructors may only return object or undefined");
          return function (t) {
            if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return t;
          }(t);
        }
        function p(t) {
          return (p = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function (t) {
            return t.__proto__ || Object.getPrototypeOf(t);
          })(t);
        }
        var y = function (t) {
          !function (t, e) {
            if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
            t.prototype = Object.create(e && e.prototype, {
              constructor: {
                value: t,
                writable: !0,
                configurable: !0
              }
            }), Object.defineProperty(t, "prototype", {
              writable: !1
            }), e && a(t, e);
          }(i, t);
          var e,
            n,
            o = f(i);
          function i(t) {
            var e,
              n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : null,
              r = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : null,
              u = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : {
                bubbles: !1,
                cancelable: !1,
                composed: !1
              };
            return l(this, i), (e = o.call(this, t, u)).message = null, e.data = null, e.message = n || t, e.data = r, e;
          }
          return e = i, (n = [{
            key: "toString",
            value: function () {
              return "[JEvent] type=".concat(this.type, " message=").concat(this.message);
            }
          }]) && c(e.prototype, n), Object.defineProperty(e, "prototype", {
            writable: !1
          }), i;
        }(i);
        function b(t) {
          return (b = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (t) {
            return typeof t;
          } : function (t) {
            return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
          })(t);
        }
        function d(t, e) {
          if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        function h(t, e) {
          return (h = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function (t, e) {
            return t.__proto__ = e, t;
          })(t, e);
        }
        function m(t) {
          var e = function () {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
              return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})), !0;
            } catch (t) {
              return !1;
            }
          }();
          return function () {
            var n,
              r = S(t);
            if (e) {
              var o = S(this).constructor;
              n = Reflect.construct(r, arguments, o);
            } else n = r.apply(this, arguments);
            return g(this, n);
          };
        }
        function g(t, e) {
          if (e && ("object" === b(e) || "function" == typeof e)) return e;
          if (void 0 !== e) throw new TypeError("Derived constructors may only return object or undefined");
          return function (t) {
            if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return t;
          }(t);
        }
        function S(t) {
          return (S = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function (t) {
            return t.__proto__ || Object.getPrototypeOf(t);
          })(t);
        }
        var w = function (t) {
          !function (t, e) {
            if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
            t.prototype = Object.create(e && e.prototype, {
              constructor: {
                value: t,
                writable: !0,
                configurable: !0
              }
            }), Object.defineProperty(t, "prototype", {
              writable: !1
            }), e && h(t, e);
          }(i, t);
          var e,
            o = m(i);
          function i() {
            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null,
              e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : null,
              n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {
                bubbles: !1,
                cancelable: !1,
                composed: !1
              };
            return d(this, i), o.call(this, "JError", t, e, n);
          }
          return e = i, Object.defineProperty(e, "prototype", {
            writable: !1
          }), e;
        }(y);
        function P(t) {
          return (P = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (t) {
            return typeof t;
          } : function (t) {
            return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
          })(t);
        }
        function j(t, e) {
          if (t !== e) throw new TypeError("Cannot instantiate an arrow function");
        }
        function O(t, e) {
          for (var n = 0; n < e.length; n++) {
            var r = e[n];
            r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(t, (o = r.key, i = void 0, i = function (t, e) {
              if ("object" !== P(t) || null === t) return t;
              var n = t[Symbol.toPrimitive];
              if (void 0 !== n) {
                var r = n.call(t, e || "default");
                if ("object" !== P(r)) return r;
                throw new TypeError("@@toPrimitive must return a primitive value.");
              }
              return ("string" === e ? String : Number)(t);
            }(o, "string"), "symbol" === P(i) ? i : String(i)), r);
          }
          var o, i;
        }
        var _ = function () {
          function t() {
            !function (t, e) {
              if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
            }(this, t), this._listeners = null, this._listeners = {};
          }
          var e, n;
          return e = t, (n = [{
            key: "addEventListener",
            value: function (t, e, n) {
              var r, o;
              this._listeners[t] || Array.isArray(this._listeners[t]) || (this._listeners[t] = []), this._listeners[t].push({
                once: null !== (r = null == n ? void 0 : n.once) && void 0 !== r && r,
                handler: e,
                capture: null !== (o = null == n ? void 0 : n.capture) && void 0 !== o && o
              });
            }
          }, {
            key: "removeEventListener",
            value: function (t, e) {
              var n = arguments.length > 2 && void 0 !== arguments[2] && arguments[2],
                r = this._listeners;
              if (r) {
                var o = r[t];
                if (o && Array.isArray(o)) for (var i = 0, u = o.length; i < u; i++) if (o[i].handler == e && o[i].capture === n) {
                  1 == u ? delete r[t] : o.splice(i, 1);
                  break;
                }
              }
            }
          }, {
            key: "removeAllEventListeners",
            value: function (t) {
              t ? this._listeners && delete this._listeners[t] : this._listeners = null;
            }
          }, {
            key: "on",
            value: function (t, e, n) {
              var r = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : null,
                o = arguments.length > 4 ? arguments[4] : void 0;
              return n = n || this, this.addEventListener(t, function (t) {
                e.call(n, t, r);
              }, o);
            }
          }, {
            key: "dispatchEvent",
            value: function (t) {
              try {
                t.target = this;
              } catch (t) {}
              if (t.bubbles) {
                for (var e = this, n = [e]; e.parent;) n.push(e = e.parent);
                var r,
                  o = n.length;
                for (r = o - 1; r >= 0 && !t.propagationStopped; r--) n[r]._dispatchEvent(t, 1 + (0 == r));
                for (r = 1; r < o && !t.propagationStopped; r++) n[r]._dispatchEvent(t, 3);
              } else this._dispatchEvent(t, 2);
            }
          }, {
            key: "_dispatchEvent",
            value: function (t, e) {
              var n,
                r,
                o,
                i,
                u,
                l = this,
                c = (n = e <= 2 ? null !== (r = null === (o = this._listeners[t.type]) || void 0 === o ? void 0 : o.filter(function (t) {
                  return j(this, l), !t.capture;
                }.bind(this))) && void 0 !== r ? r : [] : null !== (i = null === (u = this._listeners[t.type]) || void 0 === u ? void 0 : u.filter(function (t) {
                  return j(this, l), t.capture;
                }.bind(this))) && void 0 !== i ? i : []).length;
              try {
                t.currentTarget = this, t.eventPhase = 0 | e;
              } catch (t) {}
              for (var a = 0; a < c && !t.immediatePropagationStopped; a++) n[a].handler(t), n[a].once && this.removeEventListener(t.type, n[a].handler, n[a].capture);
            }
          }]) && O(e.prototype, n), Object.defineProperty(e, "prototype", {
            writable: !1
          }), t;
        }();
      }]);
    });
  })(jevent_min);
  var jevent_minExports = jevent_min.exports;

  var ResizeEvent = /*#__PURE__*/function (_JCustomEvent) {
    _inherits(ResizeEvent, _JCustomEvent);
    var _super = _createSuper(ResizeEvent);
    function ResizeEvent(data) {
      _classCallCheck(this, ResizeEvent);
      return _super.call(this, EVENTTYPE.RESIZE, "resize event", data);
    }
    return _createClass(ResizeEvent);
  }(jevent_minExports.JCustomEvent);

  var App = /*#__PURE__*/function (_JEventTarget) {
    _inherits(App, _JEventTarget);
    var _super = _createSuper(App);
    function App(container) {
      var _this;
      _classCallCheck(this, App);
      _this = _super.call(this);
      _defineProperty(_assertThisInitialized(_this), "_ready", false);
      _defineProperty(_assertThisInitialized(_this), "$el", null);
      _defineProperty(_assertThisInitialized(_this), "$canvas", null);
      var el = document.querySelectorAll(container);
      if (el && el.length === 1) {
        _this.$el = el[0];
        _this.createCanvas();
      } else {
        Logger.logCode(ERRORCODE["0C000002"]);
      }
      return _this;
    }
    _createClass(App, [{
      key: "createCanvas",
      value: function createCanvas() {
        this.$canvas = document.createElement("canvas");
        this.$el.appendChild(this.$canvas);
        this.resize(this.$el.clientWidth, this.$el.clientHeight);
        this._ready = true;
      }
    }, {
      key: "resize",
      value: function resize(width, height) {
        this.$canvas.width = width;
        this.$canvas.height = height;
        this.$canvas.style.height = height + "px";
        this.$canvas.style.width = width + "px";
        if (this._ready) {
          this.dispatchEvent(new ResizeEvent({
            width: width,
            height: height
          }));
        }
      }
    }]);
    return App;
  }(jevent_minExports.JEventTarget);

  var Cursor = /*#__PURE__*/function () {
    function Cursor() {
      _classCallCheck(this, Cursor);
    }
    _createClass(Cursor, null, [{
      key: "addCurSorUrl",
      value: function addCurSorUrl(name, url) {
        var x = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
        var y = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;
        if (url) {
          Cursor.existCurSorList[name] = "url(".concat(url, ") ").concat(x, " ").concat(y, ", auto");
        } else {
          Logger.logCode(ERRORCODE["0B000001"]);
        }
      }
    }, {
      key: "getCurSor",
      value: function getCurSor(name) {
        var curSor = Cursor.existCurSorList[name];
        if (curSor) {
          return curSor;
        } else {
          Logger.logCode(ERRORCODE["0A000001"]);
          return "auto";
        }
      }
    }]);
    return Cursor;
  }();
  _defineProperty(Cursor, "existCurSorList", {
    "auto": "auto",
    "default": "default",
    "none": "none",
    "context-menu": "context-menu",
    "help": "help",
    "pointer": "pointer",
    "progress": "progress",
    "wait": "wait",
    "cell": "cell",
    "crosshair": "crosshair",
    "text": "text",
    "vertical-text": "vertical-text",
    "alias": "alias",
    "copy": "copy",
    "move": "move",
    "no-drop": "no-drop",
    "not-allowed": "not-allowed",
    "grab": "grab",
    "grabbing": "grabbing",
    "e-resize": "e-resize",
    "n-resize": "n-resize",
    "ne-resize": "ne-resize",
    "nw-resize": "nw-resize",
    "s-resize": "s-resize",
    "se-resize": "se-resize",
    "sw-resize": "sw-resize",
    "w-resize": "w-resize",
    "ew-resize": "ew-resize",
    "ns-resize": "ns-resize",
    "nesw-resize": "nesw-resize",
    "nwse-resize": "nwse-resize",
    "col-resize": "col-resize",
    "row-resize": "row-resize",
    "all-scroll": "all-scroll",
    "zoom -in": "zoom -in",
    "zoom-out": "zoom-out"
  });

  // https://developer.mozilla.org/zh-CN/docs/Web/API/CanvasRenderingContext2D/globalCompositeOperation
  var COMPOSITEMODE = {
    SOURCEOVER: "source-over",
    SOURCEIN: "source-in",
    SOURCEOUT: "source-outn",
    SOURCEATOP: "source-atop",
    DESTINATIONOVER: "destination-over",
    DESTINATIONIN: "destination-in",
    DESTINATIONOUT: "destination-out",
    DESTINATIONATOP: "destination-atop",
    LIGHTER: "lighter",
    COPY: "copy",
    XOR: "xor",
    MULTIPLY: "multiply",
    SCREEN: "screen",
    OVERLAY: "overlay",
    DARKEN: "darken",
    LIGHTEN: "lighten",
    COLORDODGE: "color-dodge",
    COLORBURN: "color-burn",
    HARDLIGHT: "hard-light",
    SOFTLIGHT: "soft-light",
    DIFFERENCE: "difference",
    EXCLUSION: "exclusion",
    HUE: "hue",
    SATURATION: "saturation",
    COLOR: "color",
    LUMINOSITY: "luminosity"
  };
  // https://developer.mozilla.org/zh-CN/docs/Web/API/CanvasRenderingContext2D/direction
  var DIRECTION = {
    INHERIT: "inherit",
    LTR: "ltr",
    RTL: "rtl"
  };

  // https://developer.mozilla.org/zh-CN/docs/Web/API/CanvasRenderingContext2D/lineCap
  var LINECAP = {
    BUTT: "butt",
    ROUND: "round",
    SQUARE: "square"
  };

  // https://developer.mozilla.org/zh-CN/docs/Web/API/CanvasRenderingContext2D/lineJoin
  var LINEJOIN = {
    BEVEL: "bevel",
    MITER: "miter",
    ROUND: "round"
  };

  // https://developer.mozilla.org/zh-CN/docs/Web/API/CanvasRenderingContext2D/textAlign
  var TEXTALIGN = {
    CENTER: "center",
    END: "end",
    LEFT: "left",
    RIGHT: "right",
    START: "start"
  };

  //https://developer.mozilla.org/zh-CN/docs/Web/API/CanvasRenderingContext2D/textBaseline
  var TEXTBASELINE = {
    ALPHABETIC: "alphabetic",
    BOTTOM: "bottom",
    HANGING: "hanging",
    IDEOGRAPHIC: "ideographic",
    MIDDLE: "middle",
    TOP: "top"
  };

  exports.App = App;
  exports.COMPOSITEMODE = COMPOSITEMODE;
  exports.Cursor = Cursor;
  exports.DIRECTION = DIRECTION;
  exports.EVENTTYPE = EVENTTYPE;
  exports.LINECAP = LINECAP;
  exports.LINEJOIN = LINEJOIN;
  exports.TEXTALIGN = TEXTALIGN;
  exports.TEXTBASELINE = TEXTBASELINE;

}));
