import { numberEqual } from "../common/math";
import { isType } from "../common/utils";
import { ERRORCODE } from "../constants/errorCode";
import { DATATYPE, GEOMETRY } from "../constants/general";
import Logger from "../core/logger";

export default class Vector {

    /**
     * 从极坐标系创建点位
     * @param {number} raduis 
     * @param {number} angle 
     * @returns {Vector}
     */
    static formPolar(raduis, angle) {
        const point = new Vector();
        point.setX(raduis * Math.cos(angle));
        point.setY(raduis * Math.sin(angle));
        return point;
    }

    /**
     * 从2点之间内插一个值
     * @param {Vector} v1 
     * @param {Vector} v2 
     * @param {Vector} fatcor 
     * @returns 
     */
    static interpolate(v1, v2, fatcor) {
        const point = new Vector();
        point.setX(v1.getX() + fatcor * (v2.getX() - v1.getX()))
        point.setY(v1.getY() + fatcor * (v2.getY() - v1.getY()))
        return point
    }

    constructor(x = 0, y = 0) {
        this.type = GEOMETRY.VECTOR
        this.set(x, y)
    }

    set(x, y) {
        this.setX(x)
        this.setY(y)
    }

    add(x, y) {
        if (isType(x, DATATYPE.NUMBER) && isType(y, DATATYPE.NUMBER)) {
            return new Vector(this.getX() + x, this.getY() + y)
        } else {
            throw "param x and y must be instanceof Number"
        }
    }

    setX(x) {
        if (isType(x, DATATYPE.NUMBER)) {
            this.x = x;
        } else {
            Logger.logCode(ERRORCODE["0C000003"])
            throw ERRORCODE["0C000003"]
        }

    }

    setY(y) {
        if (isType(y, DATATYPE.NUMBER)) {
            this.y = y;
        } else {
            Logger.logCode(ERRORCODE["0C000003"])
            throw ERRORCODE["0C000003"]
        }

    }

    get() {
        return { x: this.x, y: this.y }
    }

    getX() {
        return this.x;
    }

    getY() {
        return this.y;
    }

    equal(vector) {
        return numberEqual(vector.getX(), this.getX()) && numberEqual(vector.getY(), this.getY())
    }

    getEuclideanDistance(vector) {
        return Math.sqrt(Math.pow(vector.getX() - this.getX(), 2) - Math.pow(vector.getY() - this.getY(), 2))
    }

    clone() {
        return new Vector(this.getX(), this.getY());
    }

    toString() {
        return this.type
    }
}