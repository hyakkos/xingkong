import { GEOMETRY, NONE } from "../constants/general";

export default class Line {
    constructor(start, end) {
        this.type = GEOMETRY.LINE;
        this.set(start, end)
    }

    set(start, end) {
        this.setStart(start);
        this.setEnd(end);
    }

    setStart(start) {
        if (start instanceof Vector) {
            if (this.end instanceof Vector && this.end.equal(start)) {
                Logger.logCode(ERRORCODE["0C000004"])
                throw ERRORCODE["0C000004"]
            } else {
                this.start = start;
            }
        } else {
            Logger.logCode(ERRORCODE["0C000003"])
            throw ERRORCODE["0C000003"]
        }
    }

    setEnd(end) {
        if (end instanceof Vector) {
            if (this.start instanceof Vector && this.start.equal(end)) {
                Logger.logCode(ERRORCODE["0C000004"])
                throw ERRORCODE["0C000004"]
            } else {
                this.end = end;
            }
        } else {
            Logger.logCode(ERRORCODE["0C000003"])
            throw ERRORCODE["0C000003"]
        }
    }

    getLength() {
        return this.start.getEuclideanDistance(this.end)
    }

    getStart() {
        return this.start;
    }

    getEnd() {
        return this.end;
    }
    /**
     * 获取直线的方程
     */
    getEquation() {
        const molecule = this.end.getY() - this.start.getY();
        const denominator = this.end.getX() - this.start.getX();
        if (denominator === 0) {
            // y = b
            return {
                slope: NONE,
                Intercept: this.end.getY()
            }
        } else if(molecule){
            // x = n
            return {
                slope: NONE,
                Intercept: NONE
            }
        } else {
            // y = kx + b
            const slope = molecule / denominator;
            return {
                slope: slope,
                Intercept: this.end.getY() - slope * this.end.getX()
            }
        }
    }

    positionRelation(line) {
        const selfEquation = this.getEquation();
        const otherEquation = line.getEquation();
    }

    clone() {
        return new Line(this.start.clone(), this.end.clone())
    }

    toString() {
        return this.type
    }
}