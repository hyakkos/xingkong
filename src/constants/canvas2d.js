// https://developer.mozilla.org/zh-CN/docs/Web/API/CanvasRenderingContext2D/globalCompositeOperation
export const COMPOSITEMODE = {
    SOURCEOVER: "source-over",
    SOURCEIN: "source-in",
    SOURCEOUT: "source-outn",
    SOURCEATOP: "source-atop",
    DESTINATIONOVER: "destination-over",
    DESTINATIONIN: "destination-in",
    DESTINATIONOUT: "destination-out",
    DESTINATIONATOP: "destination-atop",
    LIGHTER: "lighter",
    COPY: "copy",
    XOR: "xor",
    MULTIPLY: "multiply",
    SCREEN: "screen",
    OVERLAY: "overlay",
    DARKEN: "darken",
    LIGHTEN: "lighten",
    COLORDODGE: "color-dodge",
    COLORBURN: "color-burn",
    HARDLIGHT: "hard-light",
    SOFTLIGHT: "soft-light",
    DIFFERENCE: "difference",
    EXCLUSION: "exclusion",
    HUE: "hue",
    SATURATION: "saturation",
    COLOR: "color",
    LUMINOSITY: "luminosity"
}
// https://developer.mozilla.org/zh-CN/docs/Web/API/CanvasRenderingContext2D/direction
export const DIRECTION = {
    INHERIT: "inherit",
    LTR: "ltr",
    RTL: "rtl"
}

// https://developer.mozilla.org/zh-CN/docs/Web/API/CanvasRenderingContext2D/lineCap
export const LINECAP = {
    BUTT: "butt",
    ROUND: "round",
    SQUARE: "square"
}

// https://developer.mozilla.org/zh-CN/docs/Web/API/CanvasRenderingContext2D/lineJoin
export const LINEJOIN = {
    BEVEL: "bevel",
    MITER: "miter",
    ROUND: "round"
}

// https://developer.mozilla.org/zh-CN/docs/Web/API/CanvasRenderingContext2D/textAlign
export const TEXTALIGN = {
    CENTER: "center",
    END: "end",
    LEFT: "left",
    RIGHT: "right",
    START: "start"
}

//https://developer.mozilla.org/zh-CN/docs/Web/API/CanvasRenderingContext2D/textBaseline
export const TEXTBASELINE = {
    ALPHABETIC: "alphabetic",
    BOTTOM: "bottom",
    HANGING: "hanging",
    IDEOGRAPHIC: "ideographic",
    MIDDLE: "middle",
    TOP: "top"
}

// https://developer.mozilla.org/zh-CN/docs/Web/API/CanvasRenderingContext2D/imageSmoothingQuality
export const IMAGESMOOTHINGQUALITY = {
    LOW: "low",
    MEDIUM: "medium",
    HIGH: "high"
}