export const NONE = "None"

export const DATATYPE = {
    OBJECT: "Object",
    BOOLEAN: "Boolean",
    NUMBER: "Number",
    STRING: "String",
    FUNCTION: "Function",
    ARRAY: "Array",
    DATE: "Date",
    REGEXP: "Regexp",
    UNDEFINED: "Undefined",
    NULL: "Null"
}

export const GEOMETRY = {
    VECTOR: "Vector",
    LINE: "LINE"
}

export const LINEPOSITIONRELATION = {
    PARALLEL: "parallel", //平行
    INTERSECT: "intersect", // 相交
    COINCIDE: "coincide", // 重合
    VERTICAL: "vertical", // 垂直
    NONINTERSECT: "non-intersect" // 不相交
}
