import { ERRORCODE } from "./constants/errorCode";
import Logger from "./core/logger";
import ResizeEvent from "./event/resize";
import { JEventTarget } from "@hyakkos/jevent"
export default class App extends JEventTarget {
    _ready = false;

    $el = null;

    $canvas = null;

    constructor(container) {
        super();
        const el = document.querySelectorAll(container);
        if (el && el.length === 1) {
            this.$el = el[0];
            this.createCanvas();
        } else {
            Logger.logCode(ERRORCODE["0C000002"])
        }
    }
    createCanvas() {
        this.$canvas = document.createElement("canvas");
        this.$el.appendChild(this.$canvas);
        this.resize(this.$el.clientWidth, this.$el.clientHeight);
        this._ready = true;
    }

    resize(width, height) {
        this.$canvas.width = width;
        this.$canvas.height = height;
        this.$canvas.style.height = height + "px";
        this.$canvas.style.width = width + "px";
        if (this._ready) {
            this.dispatchEvent(new ResizeEvent({ width, height }));
        }
    }
}