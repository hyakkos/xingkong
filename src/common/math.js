export const DEVIATION = 0.00000001
export function numberEqual(num1, num2){
    return Math.abs(num1 - num2) <= DEVIATION;
}