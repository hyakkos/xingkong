import { EVENTTYPE } from "../constants/eventType";
import { JCustomEvent } from "@hyakkos/jevent"
export default class ResizeEvent extends JCustomEvent {
    constructor(data) {
        super(EVENTTYPE.RESIZE, "resize event", data);
    }
}