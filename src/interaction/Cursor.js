import { ERRORCODE } from "../constants/errorCode";
import Logger from "../core/logger";

export default class Cursor {
    static existCurSorList = {
        "auto": "auto",
        "default": "default",
        "none": "none",
        "context-menu": "context-menu",
        "help": "help",
        "pointer": "pointer",
        "progress": "progress",
        "wait": "wait",
        "cell": "cell",
        "crosshair": "crosshair",
        "text": "text",
        "vertical-text": "vertical-text",
        "alias": "alias",
        "copy": "copy",
        "move": "move",
        "no-drop": "no-drop",
        "not-allowed": "not-allowed",
        "grab": "grab",
        "grabbing": "grabbing",
        "e-resize": "e-resize",
        "n-resize": "n-resize",
        "ne-resize": "ne-resize",
        "nw-resize": "nw-resize",
        "s-resize": "s-resize",
        "se-resize": "se-resize",
        "sw-resize": "sw-resize",
        "w-resize": "w-resize",
        "ew-resize": "ew-resize",
        "ns-resize": "ns-resize",
        "nesw-resize": "nesw-resize",
        "nwse-resize": "nwse-resize",
        "col-resize": "col-resize",
        "row-resize": "row-resize",
        "all-scroll": "all-scroll",
        "zoom -in": "zoom -in",
        "zoom-out": "zoom-out"
    };

    static addCurSorUrl(name, url, x = 0, y = 0) {
        if (url) {
            Cursor.existCurSorList[name] = `url(${url}) ${x} ${y}, auto`
        } else {
            Logger.logCode(ERRORCODE["0B000001"])
        }
    }

    static getCurSor(name) {
        const curSor = Cursor.existCurSorList[name]
        if (curSor) {
            return curSor;
        } else {
            Logger.logCode(ERRORCODE["0A000001"])
            return "auto"
        }
    }
}