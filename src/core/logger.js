import { ERRORCODE } from "../constants/errorCode";
export default class Logger {
    static info(message) {
        console.info(message)
    }

    static warn(message) {
        console.warn(message)
    }

    static error(message) {
        console.error(message)
    }

    static log(message) {
        console.log(message)
    }

    static logCode(code) {
        if (code && code.length === 8) {
            const classify = code.slice(0, 2);
            const message = ERRORCODE[code] || undefined;
            if (message) {
                if (classify === "0A") {
                    Logger.info(`[${code}]${message}`);
                } else if (classify === "0B") {
                    Logger.warn(`[${code}]${message}`);
                } else {
                    Logger.error(`[${code}]${message}`);
                }
            } else {
                Logger.logCode(ERRORCODE["0C000001"]);
            }

        } else {
            Logger.logCode(ERRORCODE["0B000001"]);
        }
    }
}