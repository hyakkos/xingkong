import { isType } from "../common/utils";
import { COMPOSITEMODE, DIRECTION, IMAGESMOOTHINGQUALITY, LINECAP, LINEJOIN, TEXTALIGN, TEXTBASELINE } from "../constants/canvas2d";
import { ERRORCODE } from "../constants/errorCode";
import { DATATYPE } from "../constants/general";
import Logger from "./logger";

export default class Context2DStyle {
    defaultStyle = {
        direction: DIRECTION.INHERIT,
        fillStyle: "#000000",
        filter: "none",
        font: "10px sans-serif",
        globalAlpha: 1,
        globalCompositeOperation: COMPOSITEMODE.SOURCEIN,
        imageSmoothingEnabled: true,
        imageSmoothingQuality: IMAGESMOOTHINGQUALITY.LOW,
        lineCap: LINECAP.BUTT,
        lineDashOffset: 0,
        lineJoin: LINEJOIN.MITER,
        lineWidth: 1,
        miterLimit: 10,
        shadowBlur: 0,
        shadowColor: "rgba(0, 0, 0, 0)",
        shadowOffsetX: 0,
        shadowOffsetY: 0,
        strokeStyle: "#000000",
        textAlign: TEXTALIGN.START,
        textBaseline: TEXTBASELINE.ALPHABETIC
    }

    constructor(style) {
        this.styleKeys = Object.keys(this.defaultStyle);
        this.reset();
        this.setStyles(style);
    }

    reset() {
        for (key in this.defaultStyle) {
            this[key] = this.defaultStyle[key]
        }
    }

    setStyle(key, value) {
        if (this.styleKeys.includes(key)) {
            this[key] = value;
        } else {
            Logger.logCode(ERRORCODE["0A000002"])
        }
    }

    setStyles(style) {
        if (isType(style, DATATYPE.OBJECT)) {
            for (const key in style) {
                this.setStyle(key, style[key])
            }
        }
    }

    getStyle(key) {
        if (this.styleKeys.includes(key)) {
            return this[key]
        } else {
            Logger.logCode(ERRORCODE["0A000002"])
            return null
        }
    }
}